let rec append x l n = match n with
  | 0 -> l
  | a -> append x (x::l) (a-1)
;;

let rec replicate l n =
  match l with
  | [] -> []
  | a::r -> append a [] n@replicate r n ;;