let rec length l = 
  match l with
  | [] -> 0
  | a::r -> 1 + length r ;;