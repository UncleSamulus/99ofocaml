let rec k_th_element k l =
  match l with 
  | [] -> None
  | a::r -> if k=1 then a else k_th_element (k-1) l ;;