let rec encode l = 
  let rec count_consecutives x l =
    match l with
    | [] -> 0
    | a::r when a=x -> 1+(count_consecutives x r)
    | _-> 0
  in
  let rec remove_consecutive x l =
    match l with
    | [] -> []
    | a::r when a=x -> remove_consecutive x r 
    | a::r -> l
  in
  match l with
  | [] -> []
  | a::q -> (count_consecutives a l, a)::(encode (remove_consecutive a l)) ;;

