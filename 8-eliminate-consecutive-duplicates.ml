let rec compress l = 
  match l with
  | [] -> []
  | a::b::q when a=b -> compress (b::q)
  | a::q -> a::compress q  ;;