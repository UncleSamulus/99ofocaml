let range min max = 
  let rec aux i max = 
    if i < max then
      i::(aux (i+1) max)
    else 
      []
  in
    if min > max then List.rev (aux max min) else aux min max ;;
