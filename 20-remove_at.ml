let rec remove_at i list = 
  match list with
  | [] -> []
  | a::r when i-1=0 -> r 
  | a::r -> a::remove_at (i-1) r ;;