let rec insert_at i x list =
  match list with
  | [] -> []
  | a::r when i=0 -> x::a::r 
  | a::r -> a::insert_at (i-1) x r ;;