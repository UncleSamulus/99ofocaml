
let rec duplicate l = 
  match l with
  | [] -> []
  | a::r -> a::a::duplicate r ;;